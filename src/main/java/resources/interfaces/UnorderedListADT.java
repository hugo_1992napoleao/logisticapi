package resources.interfaces;

public interface UnorderedListADT<T> extends ListADT<T> {
    /**
     * Adds the specified element to this list
     * in the front
     *
     * @param element the element to be added to this list
     */
    public void addToFront(T element);
    /**
     * Adds the specified element to this list
     * in the rear
     *
     * @param element the element to be added to this list
     */
    public void addToRear(T element);
    /**
     * Adds the specified element to this list
     * after the target element
     *
     * @param element the element to be added to this list
     */
    public void addAfter(T element, T target);
}
