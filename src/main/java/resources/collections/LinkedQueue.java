package resources.collections;

import resources.exceptions.InvalidOperationException;
import resources.interfaces.QueueADT;

public class LinkedQueue<T> implements QueueADT<T> {
    private int count;
    private LinearNode<T> front;
    private LinearNode<T> rear;

    public LinkedQueue() {
        this.count = 0;
        this.rear = null;
        this.front = null;
    }

    @Override
    public void enqueue(Object element) {
        LinearNode<T> node = new LinearNode<>(element);
        if (isEmpty()) {
            this.front = node;
        } else {
            this.rear.setNext(node);
        }
        this.rear = node;
        this.count++;
    }

    @Override
    public T dequeue() {
        if (isEmpty()) {
            try {
                throw new InvalidOperationException("A queue esta vazia");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        T result = this.front.getElement();
        this.front = this.front.getNext();
        this.count--;
        if (isEmpty())
            this.rear = null;
        return result;
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            try {
                throw new InvalidOperationException("A queue esta vazia");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        return this.front.getElement();
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public int size() {
        return this.count;
    }

    public String toString() {
        String result = "";
        LinearNode<T> current = this.front;
        while (current != null) {
            result = result + (current.getElement()).toString() + "\n";
            current = current.getNext();
        }
        return result;
    }
}
