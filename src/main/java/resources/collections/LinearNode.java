package resources.collections;

public class LinearNode<T> {
    private T element;
    private LinearNode<T> next;

    public LinearNode(T element, LinearNode<T> next) {
        this.element = element;
        this.next = next;
    }

    public LinearNode(Object element) {
        this.element = (T) element;
        this.next = null;
    }

    public T getElement() {
        return element;
    }

    public void setElement(T element) {
        this.element = element;
    }

    public LinearNode<T> getNext() {
        return next;
    }

    public void setNext(LinearNode<T> next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return "LinearNode{" +
                "element=" + element +
                ", next=" + next +
                '}';
    }
}
