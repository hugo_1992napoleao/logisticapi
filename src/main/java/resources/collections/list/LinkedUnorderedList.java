package resources.collections.list;

import resources.collections.LinearNode;
import resources.exceptions.InvalidOperationException;
import resources.interfaces.UnorderedListADT;

public class LinkedUnorderedList<T> extends LinkedList<T> implements UnorderedListADT<T> {
    public LinkedUnorderedList() {
        super();
    }

    @Override
    public void addToFront(Object element) {
        LinearNode<T> newNode = new LinearNode<>(element);
        if (isEmpty()) {
            this.head = this.tail = newNode;
        } else {
            newNode.setNext(this.head);
            this.head = newNode;
        }
        this.count++;
    }

    @Override
    public void addToRear(Object element) {
        LinearNode<T> newNode = new LinearNode<>(element);
        if (isEmpty()) {
            this.head = this.tail = newNode;
        } else {
            this.tail.setNext(newNode);
            this.tail = newNode;
        }
        this.count++;
    }

    @Override
    public void addAfter(Object element, Object target) {
        if (isEmpty()) {
            try {
                throw new InvalidOperationException("A lista esta vazia");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        boolean found = false;
        LinearNode<T> current = head;
        LinearNode<T> newNode = new LinearNode<>(element);

        while (current != null && !found)
            if (target.equals(current.getElement())) {
                found = true;
            } else {
                current = current.getNext();
            }

        if (!found) {
            try {
                throw new InvalidOperationException("O elemento alvo não foi encontrado");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        newNode.setNext(current.getNext());
        current.setNext(newNode);
        count++;
    }
}
