package resources.collections.list;

import resources.collections.LinearNode;
import resources.exceptions.InvalidOperationException;
import resources.interfaces.ListADT;

import java.util.Iterator;

public class LinkedList<T> implements ListADT<T> {
    protected int count;
    protected LinearNode<T> head, tail;

    public LinkedList() {
        this.count = 0;
        this.head = this.tail = null;
    }

    @Override
    public T removeFirst() {
        if (isEmpty()) {
            try {
                throw new InvalidOperationException("A lista esta vazia");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        LinearNode<T> result = this.head;
        this.head = this.head.getNext();
        if (this.head == null)
            this.tail = null;
        return result.getElement();
    }

    @Override
    public T removeLast() {
        if (isEmpty()) {
            try {
                throw new InvalidOperationException("A lista esta vazia");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        LinearNode<T> previous = null;
        LinearNode<T> current = this.head;
        while (current.getNext() != null) {
            previous = current;
            current = current.getNext();
        }
        LinearNode<T> result = this.tail;
        this.tail = previous;
        if (this.tail == null) {
            this.head = null;
        } else {
            this.tail.setNext(null);
        }
        this.count--;
        return result.getElement();
    }

    @Override
    public T remove(Object element) {
        if (isEmpty()) {
            try {
                throw new InvalidOperationException("A lista esta vazia");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        boolean found = false;
        LinearNode<T> previous = null;
        LinearNode<T> current = this.head;
        while (current != null && !found) {
            if (element.equals(current.getElement())) {
                found = true;
            } else {
                previous = current;
                current = current.getNext();
            }
        }
        if (!found) {
            try {
                throw new InvalidOperationException("O elemento não foi encontado");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        if (size() == 1) {
            this.head = this.tail = null;
        } else if (current.equals(this.head)) {
            this.head = current.getNext();
        } else if (current.equals(this.tail)) {
            this.tail = previous;
            this.tail.setNext(null);
        } else {
            previous.setNext(current.getNext());
        }
        this.count--;
        return current.getElement();
    }

    @Override
    public T first() {
        return this.head.getElement();
    }

    @Override
    public T last() {
        return this.tail.getElement();
    }

    @Override
    public boolean contains(Object target) {
        if (isEmpty()) {
            try {
                throw new InvalidOperationException("A lista esta vazia");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        boolean found = false;
        LinearNode<T> current = this.head;
        while (current != null && !found) {
            if (target.equals(current.getElement()))
                found = true;
            else {
                current = current.getNext();
            }
        }
        return found;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedIterator<>(this.head);
    }

    @Override
    public String toString() {
        LinearNode<T> current = head;
        String result = "";
        while (current != null) {
            result = result + (current.getElement()).toString() + "\n";
            current = current.getNext();
        }
        return result;
    }

    private static class LinkedIterator<T> implements Iterator<T> {
        private LinearNode<T> current;

        public LinkedIterator(LinearNode<T> collection) {
            this.current = collection;
        }

        @Override
        public boolean hasNext() {
            return this.current != null;
        }

        @Override
        public T next() {
            if (!hasNext()) {
                try {
                    throw new InvalidOperationException("Nao exite mais elementos");
                } catch (InvalidOperationException e) {
                    e.printStackTrace();
                }
            }
            T result = this.current.getElement();
            this.current = this.current.getNext();
            return result;
        }
    }
}
