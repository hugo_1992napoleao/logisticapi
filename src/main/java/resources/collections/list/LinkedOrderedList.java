package resources.collections.list;

import resources.collections.LinearNode;
import resources.interfaces.OrderedListADT;

public class LinkedOrderedList<T> extends LinkedList<T> implements OrderedListADT<T> {
    public LinkedOrderedList() {
        super();
    }

    @Override
    public void add(Object element) {
        LinearNode current;
        Comparable<T> temp = (Comparable<T>) element;

        if (this.head == null || temp.compareTo(this.head.getElement()) >= 0) {
            this.head = new LinearNode(element, this.head);
        } else {
            /* Locate the node before point of insertion. */
            current = this.head;

            while (current.getNext() != null && temp.compareTo((T) current.getNext().getElement()) < 0) {
                current = current.getNext();
            }
            LinearNode aux = new LinearNode(element, current.getNext());
            current.setNext(aux);
        }
        this.count++;
    }
}
