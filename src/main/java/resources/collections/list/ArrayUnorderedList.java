package resources.collections.list;

import resources.exceptions.InvalidOperationException;
import resources.interfaces.UnorderedListADT;

public class ArrayUnorderedList<T> extends ArrayList<T> implements UnorderedListADT<T> {

    public ArrayUnorderedList() {
        super();
    }


    public void addToFront(T t) {
        if(isFull()){
            expandCapacity();
        }

        for (int i = last; i > 0; --i) {
            list[i] = list[i - 1];
        }

        list[0] = t;
        ++last;
        nElements++;
    }


    public void addToRear(T t) {
        if(isFull()){
            expandCapacity();
        }

        if (size() == list.length) {
            expandCapacity();
        }

        list[last] = t;
        ++last;
        nElements++;
    }


    public void addAfter(T t, T t1)  {
        if(isFull()){
            expandCapacity();
        }

        int i = 0;

        while (i < last && !t1.equals(list[i])) {
            ++i;
        }

        if (i == last) {
            try {
                throw new InvalidOperationException("A lista esta cheia");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        ++i;
        for (int k = last; k > i; --k) {
            list[k] = list[k - 1];
        }

        list[i] = t;
        ++last;
        nElements++;
    }

}
