package resources.collections.list;


import resources.exceptions.InvalidOperationException;
import resources.interfaces.ListADT;
import java.util.Iterator;

public class ArrayList<T> implements ListADT<T> {
    private final int DEFAULT_CAPACITY = 15;
    private int first;
    protected int last;
    private int position;
    protected T[] list;
    protected int nElements;

    /**
     * Metodo construtor que instanceia a classe.({@link ArrayList})
     */
    public ArrayList() {
        this.first = 0;
        this.last = 0;
        this.position = -1;
        this.list = (T[]) new Object[this.DEFAULT_CAPACITY];
        this.nElements = 0;
    }

    /**
     * Metodo construtor que instanceia a classe.({@link ArrayList})
     * @param initialCapacity
     */
    public ArrayList(int initialCapacity) {
        this.first = 0;
        this.last = 0;
        this.position = -1;
        this.list = (T[]) new Object[initialCapacity];
        this.nElements = 0;
    }

    public T removeFirst() {
        T result = null;

        if (isEmpty()) {
            try {
                throw new InvalidOperationException("A Lista esta vazia");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        } else {
            result = this.list[0];
            --(this.last);
            for (int i = 0; i < this.last; ++i) {
                this.list[this.last] = this.list[i + 1];
            }

            this.list[this.last] = null;
        }
        return result;
    }

    public T removeLast() {
        T result = null;

        if (isEmpty()) {
            try {
                throw new InvalidOperationException("A lista esta vazia");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        } else {
            --(this.last);
            result = this.list[this.last];
            this.list[this.last] = null;
        }
        return result;
    }

    public T remove(Object element) {
        T result = null;

        if (!contains(element)) {
            try {
                throw new InvalidOperationException("Elemento não foi encontrado");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        } else {
            result = this.list[this.position];
            for(int i=this.position; i<this.last; i++){
                if(i+1 < this.last){
                    this.list[i] = this.list[i + 1];
                }
            }
            --(this.last);
            this.list[this.last] = null;
        }
        return result;
    }


    public T first() {
        if (isEmpty()) {
            try {
                throw new InvalidOperationException("A lista esta vazia");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        } else {
            return this.list[this.first];
        }
        return null;
    }

    public T last() {
        if (isEmpty()) {
            try {
                throw new InvalidOperationException("A lista esta vazia");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        } else {
            return this.list[this.last - 1];
        }
        return null;
    }

    public boolean contains(Object target) {
        boolean contain = false;
        int i = 0;

        if (!isEmpty()) {
            while (!contain && i < this.last) {
                if (target.equals(this.list[i])) {
                    contain = true;
                    this.position = i;
                } else {
                    ++i;
                }
            }
        }
        return contain;
    }

    public boolean isEmpty() {

        return size() == 0;
    }

    /**
     * Metodo que devolve o tamanho da lista.({@link ArrayList})
     * @return
     */
    public int size() {
        int count = 0;

        for (int i = 0; i < this.list.length; ++i) {
            if (this.list[i] != null) {
                ++count;
            }
        }
        return count;
    }

    /**
     * Iterador da lista.({@link ArrayList})
     * @return
     */
    public Iterator<T> iterator() {
        BasicIterator<T> bi = new BasicIterator<>();
        return bi;
    }

    @Override
    public String toString() {
        String result = "";

        for (int i = 0; i < size(); i++) {
            result = result + this.list[i].toString();
        }

        return result;
    }

    /**
     * Metodo que verifica se a lista está cheia.({@link ArrayList})
     * @return
     */
    public boolean isFull(){
        int count = this.last;
        return count == this.list.length;
    }

    /**
     * Metodo que aumenta o tamanho da lista.({@link ArrayList})
     */
    public void expandCapacity(){
        T[] temp = (T[]) (new Object[this.list.length + 1]);

        for (int i = 0; i < this.list.length; i++) {
            temp[i] = this.list[i];
        }
        this.list = temp;
        this.last = temp.length - 1;
    }

    /**
     * Método getter do listSize.({@link ArrayList})
     *
     * @return listSize
     */
    public int getlistSize() {
        return this.list.length;
    }

    /**
     * Método getter do list.({@link ArrayList})
     *
     * @return list
     */
    public T[] getList() {
        return list;
    }

    /**
     * Método setter do list.({@link ArrayList})
     *
     * @param list
     */
    public void setList(T[] list) {
        this.list = list;
    }

    /**
     * Método getter do first.({@link ArrayList})
     *
     * @return first
     */
    public int getFirst() {
        return first;
    }

    /**
     * Método setter do first.({@link ArrayList})
     *
     * @param first
     */
    public void setFirst(int first) {
        this.first = first;
    }

    /**
     * Método getter do last.({@link ArrayList})
     *
     * @return last
     */
    public int getLast() {
        return last;
    }

    /**
     * Método setter do last.({@link ArrayList})
     *
     * @param last
     */
    public void setLast(int last) {
        this.last = last;
    }

    /**
     * Método getter do nElements.({@link ArrayList})
     *
     * @return nElements
     */
    public int getnElements() {
        return nElements;
    }

    /**
     * Método setter do nElements.({@link ArrayList})
     *
     * @param nElements
     */
    public void setnElements(int nElements) {
        this.nElements = nElements;
    }

    /**
     * Classe do iterator.({@link ArrayList})
     *
     * @param <T>
     */
    public class BasicIterator<T> implements Iterator<T> {
        private int cursor;


        /**
         * Metodo construtor que instanceia a classe.({@link BasicIterator})
         */
        public BasicIterator(){
            cursor = 0;
        }


        /**
         * Metodo que percorre o iterator.({@link BasicIterator})
         */
        @Override
        public boolean hasNext() {

            if(cursor < ArrayList.this.getList().length && ArrayList.this.getList()[cursor] != null){
                return this.cursor < ArrayList.this.getLast();
            }
            return false;
        }

        /**
         * Metodo que devolve o objeto onde se encontra o iterator.({@link BasicIterator})
         */
        @Override
        public T next() {
            cursor++;
            return (T) ArrayList.this.getList()[cursor-1];
        }

        /**
         * Método getter do cursor.({@link BasicIterator})
         *
         * @return cursor
         */
        public int getCursor() {
            return cursor;
        }

        /**
         * Método setter do cursor.({@link BasicIterator})
         *
         * @param cursor
         */
        public void setCursor(int cursor) {
            this.cursor = cursor;
        }
    }

}
