package resources.collections;

import resources.exceptions.InvalidOperationException;
import resources.interfaces.StackADT;

public class ArrayStack<T> implements StackADT<T> {
    private final static int DEFAULT_SIZE = 10;
    private T[] stack;
    private int count;

    public ArrayStack() {
        this.stack = (T[]) new Object[DEFAULT_SIZE];
        this.count = 0;
    }

    @Override
    public void push(Object element) {
        if(size() == this.stack.length)
            expandCapacity();
        this.stack[size()] = (T) element;
        this.count++;
    }

    private void expandCapacity() {
        Object[] array = new Object[this.stack.length * 2];
        for (int i = 0; i < size(); i++) {
            array[i] = this.stack[i];
        }
        this.stack = (T[]) array;
    }

    @Override
    public T pop() {
        if (isEmpty()) {
            try {
                throw new InvalidOperationException("A stack esta vazia");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        Object result = this.stack[size() - 1];
        this.stack[size()] = null;
        this.count--;
        return (T) result;
    }

    @Override
    public T peek() {
        if(isEmpty()) {
            try {
                throw new InvalidOperationException("A stack esta vazia");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        return this.stack[size()];
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public int size() {
        return this.count;
    }
}
