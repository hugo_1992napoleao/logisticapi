package resources.collections.Graph;


import Logic.Location;
import resources.collections.list.*;
import resources.exceptions.InvalidOperationException;
import resources.interfaces.*;

import java.util.Iterator;

public class Graph<T> implements GraphADT<T> {
    private static final int DEFAULT_CAPACITY = 1;
    protected int numVertices; // number of vertices in the graph
    protected UnorderedListADT<Edges>[] adjList; // adjacency matrix
    protected UnorderedListADT<Location> vertices; // values of vertices

    /**
     * initializes the grafo class
     */
    public Graph() {
        this.vertices = new LinkedUnorderedList<>();
        this.adjList = new LinkedUnorderedList[DEFAULT_CAPACITY];
        this.numVertices = 0;
    }

    @Override
    public void addVertex(Object vertex) {
        if (this.adjList.length == this.numVertices)
            ExpandCapacity();
        this.vertices.addToFront((Location) vertex);
        this.adjList[this.numVertices] = new LinkedUnorderedList<>();
        this.numVertices++;
    }

    /**
     * Increases the size of the list of adjacency
     */
    private void ExpandCapacity() {
        UnorderedListADT<Edges>[] newAdjList = new LinkedUnorderedList[this.numVertices + 1];

        for (int i = 0; i < size(); i++) {
            newAdjList[i] = this.adjList[i];
        }

        this.adjList = newAdjList;
    }

    @Override
    public void removeVertex(Object vertex) {
        Object temp;
        temp = this.vertices.remove((Location) vertex);
        if (temp != null)
            this.numVertices--;
    }

    /**
     * Finds the position of a vertex in the list of vertex
     *
     * @param vertex - Element to be found
     * @return position in the array of vertex, -1 if element is not found
     */
    public int findVertex(Object vertex) {
        int i = -1;
        Iterator<Location> itr = this.vertices.iterator();
        while (itr.hasNext()) {
            i++;
            Location temp = itr.next();
            if (vertex.toString().equals(temp.toString()))
                return i;
        }
        return -1;
    }

    @Override
    public void addEdge(Object vertex1, Object vertex2) {
        int pos1 = findVertex(vertex1);
        int pos2 = findVertex(vertex2);
        if (!indexIsValid(pos1) || !indexIsValid(pos2)) {
            try {
                throw new InvalidOperationException("O vertice não foi encontrado");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        this.adjList[pos1].addToFront(new Edges((String) vertex2, 0));
    }

    @Override
    public void removeEdge(Object vertex1, Object vertex2) {
        int pos1 = findVertex(vertex1);
        int pos2 = findVertex(vertex2);
        if (!indexIsValid(pos1) || !indexIsValid(pos2)) {
            try {
                throw new InvalidOperationException("O vertice não foi encontrado");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        Iterator<Edges> itr = this.adjList[pos1].iterator();
        boolean sucess = false;
        while (itr.hasNext()) {
            Edges temp = itr.next();
            if (temp.getDestino().equals(vertex2)) {
                this.adjList[pos1].remove(new Edges(temp.getDestino(), temp.getPeso()));
                sucess = true;
            }
        }
        if (!sucess) {
            try {
                throw new InvalidOperationException("Não existe ligação entre os vertices:\n" + vertex1.toString() + "\t" + vertex2.toString());
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }
        }
    }

    public double getLigacaoValor(int i, int j) {
        Iterator<Edges> ligacoes = this.adjList[i].iterator();
        Iterator<Location> verticesIterator = this.vertices.iterator();
        Location vertices = null;
        for (int k = 0; k <= j; k++) {
            vertices = verticesIterator.next();
        }

        Edges aresta;
        while (ligacoes.hasNext()){
            aresta = ligacoes.next();
            if (aresta.getDestino().equals(vertices.getName())) {
                return aresta.getPeso();
            }
        }
        return Double.POSITIVE_INFINITY;
    }

    public String GetDivisãobyIndex(int pos) {
        Iterator<Location> iterator = this.vertices.iterator();
        Location result = null;
        for (int i = 0; i <= pos; i++) {
            result = iterator.next();
        }
        return result.getName();
    }
    
    @Override
    public Iterator<T> iteratorBFS(Object startVertex) {
        return null;
    }

    boolean indexIsValid(int startIndex) {
        return startIndex >= 0 && startIndex < size();
    }

    @Override
    public Iterator<T> iteratorDFS(Object startVertex) {
        return null;
    }

    @Override
    public Iterator<T> iteratorShortestPath(Object startVertex, Object targetVertex) throws InvalidOperationException {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean isConnected() {
        int size = 0;
        int posStart = findVertex("entrada");
        Iterator<T> iterator = iteratorBFS(posStart);
        while (iterator.hasNext()) {
            size++;
            iterator.next();
        }
        return size == size();
    }

    @Override
    public int size() {
        return this.numVertices;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        int i = 1;
        Iterator<Location> itrVertices = this.vertices.iterator();

        for (int j = 0; j < size(); j++) {
            Iterator<Edges> itrArestas = this.adjList[j].iterator();
            result.append(i).append("º ").append(itrVertices.next().toString()).append("\n");
            result.append("Ligações:\n");
            while (itrArestas.hasNext()) {
                result.append(itrArestas.next().toString()).append("\n");
            }
            i++;
            result.append("\n");
        }
        return result.toString();
    }
}
