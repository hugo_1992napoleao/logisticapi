package resources.collections.Graph;

import Logic.Location;
import resources.interfaces.*;

import java.util.Iterator;

public class Network<T> extends Graph<T> implements NetworkADT<T> {

    @Override
    public void addEdge(T vertex1, T vertex2, double weight) {

    }

    @Override
    public double shortestPathWeight(T vertex1, T vertex2) {
        return 0;
    }

    public Location FindyName (String name) {
        Iterator<Location> itr = this.vertices.iterator();

        while (itr.hasNext()) {
            Location temp = itr.next();
            if (name.equals(temp.getName())) {
                return temp;
            }
        }
        return null;
    }
}
