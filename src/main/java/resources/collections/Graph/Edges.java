package resources.collections.Graph;

/**
 * Class that represents a
 * edge of the graph
 */
public class Edges {
    private String destino;
    private double peso;

    public Edges(String destino, double peso) {
        this.destino = destino;
        this.peso = peso;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    @Override
    public String toString() {
        return "Arestas{" +
                "destino='" + destino + '\'' +
                ", peso=" + peso +
                '}';
    }
}
