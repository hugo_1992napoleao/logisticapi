package resources.collections.Tree;

import resources.collections.list.LinkedUnorderedList;
import resources.exceptions.InvalidOperationException;
import resources.interfaces.BinaryTreeADT;
import resources.interfaces.UnorderedListADT;

import java.util.Iterator;

public class LinkedBinaryTree<T> implements BinaryTreeADT<T> {
    protected int count;
    protected BinaryTreeNode<T> root;

    /**
     * Creates an empty binary tree.
     */
    public LinkedBinaryTree() {
        count = 0;
        root = null;
    }

    /**
     * Creates a binary tree with the specified element as its root.
     *
     * @param element the element that will become the root of the
     *                new binary tree
     */
    public LinkedBinaryTree(T element) {
        count = 1;
        root = new BinaryTreeNode<T>(element);
    }

    /**
     * Returns a reference to the specified target element if it is
     * found in this binary tree. Throws a NoSuchElementException if
     * the specified target element is not found in the binary tree.
     *
     * @param targetElement the element being sought in this tree
     * @return a reference to the specified target
     */
    public T find(T targetElement) {
        BinaryTreeNode<T> current = findAgain(targetElement, root);

        if (current == null)
            try {
                throw new InvalidOperationException("binary tree");
            } catch (InvalidOperationException e) {
                e.printStackTrace();
            }

        return (current.element);
    }

    /**
     * Returns a reference to the specified target element if it is
     * found in this binary tree.
     *
     * @param targetElement the element being sought in this tree
     * @param next          the element to begin searching from
     */
    private BinaryTreeNode<T> findAgain(T targetElement, BinaryTreeNode<T> next) {
        if (next == null)
            return null;

        if (next.element.equals(targetElement))
            return next;

        BinaryTreeNode<T> temp = findAgain(targetElement, next.left);

        if (temp == null)
            temp = findAgain(targetElement, next.right);

        return temp;
    }

    @Override
    public T getRoot() {
        return this.root.element;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public boolean contains(T targetElement) {
        T temp;
        boolean found = false;

        try {
            temp = find(targetElement);
            found = true;
        } catch (Exception ElementNotFoundException) {
            found = false;
        }

        return found;
    }

    /**
     * Performs a recursive inorder traversal.
     *
     * @param node     the node to be used as the root
     *                 for this traversal
     * @param tempList the temporary list for use in this traversal
     */
    protected void inorder(BinaryTreeNode<T> node, UnorderedListADT<T> tempList) {
        if (node != null) {
            inorder(node.left, tempList);
            tempList.addToRear(node.element);
            inorder(node.right, tempList);
        }
    }

    @Override
    public Iterator<T> iteratorInOrder() {
        UnorderedListADT<T> tempList = new LinkedUnorderedList<>();
        inorder(root, tempList);

        return tempList.iterator();
    }

    @Override
    public Iterator<T> iteratorPreOrder() {
        UnorderedListADT<T> templist = new LinkedUnorderedList<>();
        preorder(root, templist);
        return templist.iterator();
    }

    protected void preorder(BinaryTreeNode<T> node, UnorderedListADT<T> templist) {
        if (node != null) {
            templist.addToRear(node.element);
            preorder(node.left, templist);
            preorder(node.right, templist);
        }
    }

    @Override
    public Iterator<T> iteratorPostOrder() {
        UnorderedListADT<T> templist = new LinkedUnorderedList<>();
        postorder(root, templist);
        return templist.iterator();
    }

    protected void postorder(BinaryTreeNode<T> node, UnorderedListADT<T> templist) {
        if (node != null) {
            postorder(node.left, templist);
            postorder(node.right, templist);
            templist.addToRear(node.element);
        }
    }

    @Override
    public Iterator<T> iteratorLevelOrder() {
        UnorderedListADT<T> nodes = new LinkedUnorderedList<>();
        UnorderedListADT<T> templist = new LinkedUnorderedList<>();
        BinaryTreeNode<T> current;

        nodes.addToRear(root.element);

        while (!nodes.isEmpty()) {
            current = (BinaryTreeNode<T>) nodes.removeFirst();

            if (current != null) {
                templist.addToRear(current.element);
                nodes.addToRear(current.left.element);
                nodes.addToRear(current.right.element);
            }//if
            else
                templist.addToRear(null);
        }//while
        return templist.iterator();
    }
}
