package Logic;

import resources.collections.LinkedQueue;
import resources.interfaces.QueueADT;

public class Sellers<T> {
    private int id;
    private String name;
    private int carryCapacity;
    private int currentCapacity;
    private QueueADT<T> Markets;

    public Sellers(int id, String name, int carryCapacity) {
        this.id = id;
        this.name = name;
        this.carryCapacity = carryCapacity;
        Markets = new LinkedQueue<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCarryCapacity() {
        return carryCapacity;
    }

    public void setCarryCapacity(int carryCapacity) {
        this.carryCapacity = carryCapacity;
    }

    public QueueADT<T> getMarkets() {
        return Markets;
    }

    public int getCurrentCapacity() {
        return currentCapacity;
    }

    public void setCurrentCapacity(int currentCapacity) {
        this.currentCapacity = currentCapacity;
    }

    /**
     * Adds a client to the list of markets
     * @param client market to be added
     */
    public void addClient(T client) {
        this.Markets.enqueue(client);
    }

    public T removeClient() {
        return this.Markets.dequeue();
    }

    @Override
    public String toString() {
        return "Sellers{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", carryCapacity=" + carryCapacity + " in Kg" +
                ", Markets=" + Markets +
                '}';
    }
}
