package Logic;

public class Warehouse extends Location {
    private int MaxCapacity;
    private int CurrentCapacity;

    public Warehouse(String name, int maxCapacity, int currentCapacity) {
        super(name);
        MaxCapacity = maxCapacity;
        CurrentCapacity = currentCapacity;
    }

    public int getMaxCapacity() {
        return MaxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        MaxCapacity = maxCapacity;
    }

    public int getCurrentCapacity() {
        return CurrentCapacity;
    }

    public void setCurrentCapacity(int currentCapacity) {
        CurrentCapacity = currentCapacity;
    }

    @Override
    public String toString() {
        return "Warehouse{" +
                "name=" + super.toString() +
                "MaxCapacity=" + MaxCapacity +
                ", CurrentCapacity=" + CurrentCapacity +
                '}';
    }
}
