package Logic;

import resources.collections.LinkedQueue;
import resources.interfaces.QueueADT;

public class Market<T> extends Location{
    private final QueueADT<Integer> clients = new LinkedQueue<>();

    public Market(String name) {
        super(name);
    }

    /**
     * this method will add a new client to the list of clients
     * @param client value of the supplies the client desires
     */
    public void addClient(int client) {
        this.clients.enqueue(client);
    }

    /**
     * this method will remove the 1st element of the queue of clients
     */
    public void removeClient() {
        this.clients.dequeue();
    }

    public QueueADT<Integer> getClients() {
        return clients;
    }

    @Override
    public String toString() {
        return "Market{" + "\n" +
                "name=" + super.getName() + "\n" +
                "clients=" + clients + "\n" +
                '}';
    }
}
