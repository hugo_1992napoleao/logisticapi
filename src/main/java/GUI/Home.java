package GUI;

import JSON.ReadJSON;
import Logic.Location;
import Logic.Sellers;
import resources.collections.ArrayStack;
import resources.collections.Graph.Network;
import resources.interfaces.StackADT;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

public class Home extends JFrame{
    private JButton manegeSellersButton;
    private JPanel rootPanel;
    private Network<Location> graph = new Network<>();
    private ReadJSON loadGraph = new ReadJSON("src/main/resources/Mapas/Rotas.json");

    public Home(String title) {
        super(title);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(rootPanel);
        this.pack();


        Iterator<Location> itr = this.loadGraph.getListLocation();
        while (itr.hasNext()) {
            this.graph.addVertex(itr.next());
        }

        StackADT<ReadJSON.Routes> edges = this.loadGraph.getRoutes();
        while (!edges.isEmpty()) {
            ReadJSON.Routes temp = edges.pop();
            this.graph.addEdge(this.graph.FindyName(temp.getOrigin()),this.graph.FindyName(temp.getDestination()),temp.getDistance());
        }

        manegeSellersButton.addActionListener(e -> {
            dispose();
            JFrame frame = new SellersWindows(this.loadGraph.getListSellers());
            frame.setVisible(true);
        });
    }

    public static void main(String[] args) {
        JFrame frame = new Home("Home");
        frame.setVisible(true);
    }
}
