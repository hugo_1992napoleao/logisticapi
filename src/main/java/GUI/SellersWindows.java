package GUI;

import Logic.Sellers;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

public class SellersWindows extends JFrame{
    private JPanel panel1;
    private JButton editSellerDataButton;
    private JButton exportSellerDataButton;
    private JList SelectSeller;
    Iterator<Sellers> listSellers;

    public SellersWindows(Iterator<Sellers> listSellers) {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(panel1);
        this.pack();
        this.listSellers = listSellers;

        DefaultListModel<String> listModel = new DefaultListModel<>();
        while (this.listSellers.hasNext()) {
            Sellers temp = this.listSellers.next();
            listModel.addElement(temp.getName());
        }
        SelectSeller.setModel(listModel);



        editSellerDataButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new Home("Sellers Windows");
        frame.setVisible(true);
    }
}
