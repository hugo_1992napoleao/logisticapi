package JSON;

import Logic.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import resources.collections.ArrayStack;
import resources.collections.list.LinkedUnorderedList;
import resources.interfaces.QueueADT;
import resources.interfaces.StackADT;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import resources.interfaces.UnorderedListADT;


import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;

public class ReadJSON<T> {
    private String filePath;
    private UnorderedListADT<Sellers> listSellers = new LinkedUnorderedList<>();
    private UnorderedListADT<Location> listLocation = new LinkedUnorderedList<>();
    private StackADT<Routes> routes = new ArrayStack<>();

    public ReadJSON(String filePath) {
        this.filePath = filePath;
        this.getAllSellers();
        this.getAllLocations();
        this.getAllRoutes();
    }

    /**
     * Gets all sellers from the json
     */
    public void getAllSellers() {
        JSONParser jsonParser = new JSONParser();
        try (Reader reader = new FileReader(this.filePath)) {
            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
            JSONArray jsonArray = (JSONArray) jsonObject.get("vendedores");

            for (Object sellers: jsonArray) {
                JSONObject temp = (JSONObject) sellers;
                Sellers seller = new Sellers<>(Integer.parseInt(temp.get("id").toString()), temp.get("nome").toString(), Integer.parseInt(temp.get("capacidade").toString()));
                JSONArray clientsArray = (JSONArray) temp.get("mercados_a_visitar");
                for (Object i : clientsArray) {
                    String client = i.toString();
                    seller.addClient(client);
                }
                listSellers.addToFront(seller);
            }
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }

    }

    public Iterator<Sellers> getListSellers() {
        return listSellers.iterator();
    }

    /**
     * Gets all locations from JSON
     */
    public void getAllLocations() {
        JSONParser jsonParser = new JSONParser();
        try (Reader reader = new FileReader(this.filePath)) {
            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
            JSONArray jsonArray = (JSONArray) jsonObject.get("locais");

            for(Object o: jsonArray) {
                JSONObject temp = (JSONObject) o;
                if (temp.get("tipo").equals("Sede")){
                    listLocation.addToFront(new HeadQuarters(temp.get("nome").toString()));
                } else if (temp.get("tipo").equals("Mercado")) {
                    Market<T> market = new Market<>(temp.get("nome").toString());
                    JSONArray clientArray = (JSONArray) temp.get("clientes");
                    for(int i = 0; i < clientArray.size(); i++) {
                        market.addClient(Integer.parseInt(clientArray.get(i).toString()));
                    }
                    listLocation.addToFront(market);
                } else if (temp.get("tipo").equals("Armazém")) {
                    listLocation.addToFront(new Warehouse(temp.get("nome").toString(), Integer.parseInt(temp.get("capacidade").toString()), Integer.parseInt(temp.get("stock").toString())));
                }
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    public Iterator<Location> getListLocation() {
        return listLocation.iterator();
    }

    /**
     * Gets all routes from JSON
     */
    public void getAllRoutes() {
        JSONParser jsonParser = new JSONParser();
        try (Reader reader = new FileReader(this.filePath)) {
            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
            JSONArray jsonArray = (JSONArray) jsonObject.get("caminhos");
            for (Object routes: jsonArray) {
                JSONObject temp = (JSONObject) routes;
                this.routes.push(new Routes(temp.get("de").toString(), temp.get("para").toString(), Integer.parseInt(temp.get("distancia").toString())));
            }
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }

    public StackADT<Routes> getRoutes() {
        return routes;
    }

    public class Routes {
    private String origin;
    private String destination;
    private int distance;

    public Routes(String origin, String destination, int distance) {
        this.origin = origin;
        this.destination = destination;
        this.distance = distance;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public int getDistance() {
        return distance;
    }

    @Override
    public String toString() {
        return "Routes{" +
                "origin='" + origin + '\'' +
                ", destination='" + destination + '\'' +
                ", distance=" + distance +
                '}';
    }
}
}
